REAL FUNCTION f(x)
	REAL :: x
	f = x * x * x * 3 + x * x - 3 * x
END FUNCTION

PROGRAM euler
	INTEGER i
	REAL x0, x1, x2, f0, f1, f2, e0, ex, wynik
	x1 = -2.0
	x2 = 1.0
	f1 = f(x1)
	f2 = f(x2)
	e0 = 1.0e-4
	ex = 1.0e-5
	DO WHILE ((i > 0).AND.(ABS(x1 - x2) > ex))
		IF (ABS(f1 - f2) < e0) THEN
			WRITE(*, *) "Zle punkty poczatkowe"
			STOP
		END IF
		x0 = x1 - (f1 * (x1 - x2) / (f1 - f2))
		f0 = f(x0)
		IF (ABS(f0) < e0) THEN
			wynik = x0
			EXIT
		END IF
		x2 = x1
		f2 = f1
		x1 = x0
		f1 = f0
		i = i - 1
		IF (i == 0) THEN
			WRITE(*, *) "Przekroczona liczba obiegów"
			STOP
		END IF
	END DO
	WRITE(*, *) wynik
END PROGRAM euler
